<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
$app = new \Slim\App();
$container = $app->getContainer();

include 'config/views.php';
include 'config/database.php';

/* NOSSAS VIEWS SÃO DECLARADAS AQUI DENTRO */

// Roda o código
$app->run();